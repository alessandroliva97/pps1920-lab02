package u02

import math.Pi

object Exercise7 {

  trait Shape {
    // def getPerimeter: Double
    // def getArea: Double
  }
  case class Rectangle(height: Double, width: Double) extends Shape {
    require(height > 0 && width > 0)
    // override def getPerimeter(): Double = 2 * (height + width)
    // override def getArea(): Double = height * width
  }
  case class Circle(radius: Double) extends Shape {
    require(radius > 0)
    // override def getPerimeter(): Double = 2 * radius * Pi
    // override def getArea(): Double = radius * radius * Pi
  }
  case class Square(side: Double) extends Shape {
    require(side > 0)
    // override def getPerimeter(): Double = 4 * side
    // override def getArea(): Double = side * side
  }

  object Operations {

    def getPerimeter(shape: Shape): Double = shape match {
      case Rectangle(height, width) => 2 * (height + width)
      case Circle(radius) => 2 * radius * Pi
      case Square(side) => 4 * side
    }

    def getArea(shape: Shape): Double = shape match {
      case Rectangle(height, width) => height * width
      case Circle(radius) => radius * radius * Pi
      case Square(side) => side * side
    }

  }

}
