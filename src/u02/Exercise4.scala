package u02

object Exercise4 {

  val curriedMinorEvaluationLambda: Int => Int => Int => Boolean = x => y => z => x <= y && y <= z

  val minorEvaluationLambda: (Int, Int, Int) => Boolean = (x, y, z) => x <= y && y <= z

  def curriedMinorEvaluationMethod(x: Int)(y: Int)(z: Int): Boolean = x <= y && y <= z

  def minorEvaluationMethod(x: Int, y: Int, z: Int): Boolean = x <= y && y <= z

}
