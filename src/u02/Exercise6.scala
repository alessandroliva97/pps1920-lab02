package u02

import scala.annotation.tailrec

object Exercise6 {

  def fibonacci(index: Int): Int = {
    @tailrec def tailRecursion(index: Int, previous: Int, current: Int): Int = index match {
      case 0 => current
      case 1 => previous
      case _ => tailRecursion(index - 1, previous + current, previous)
    }
    tailRecursion(index, 1, 0)
  }

}
