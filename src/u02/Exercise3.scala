package u02

object Exercise3 {

  /*
   * Esercizio 3a
   */

  // val lambda
  val parityLambda: Int => String = {
    case x if x % 2 == 0 => "even"
    case _ => "odd"
  }

  // method syntax
  def parityMethod(x: Int): String = x % 2 match {
    case 0 => "even"
    case _ => "odd"
  }

  /*
   * Esercizio 3b
   */

  // val lambda
  val negLambda: (String => Boolean) => String => Boolean = predicate => !predicate(_)

  // method syntax
  def negMethod(predicate: String => Boolean) : String => Boolean = !predicate(_)

  /*
   * Esercizio 3c
   */

  // method syntax
  def negMethodGeneric[A](predicate: A => Boolean) : A => Boolean = !predicate(_)

}