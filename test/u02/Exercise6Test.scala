package u02

import u02.Exercise6._

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class Exercise6Test {

  @Test def testFibonacci0() {
    assertEquals(0, fibonacci(0))
  }

  @Test def testFibonacci1() {
    assertEquals(1, fibonacci(1))
  }

  @Test def testFibonacci2() {
    assertEquals(1, fibonacci(2))
  }

  @Test def testFibonacci3() {
    assertEquals(2, fibonacci(3))
  }

  @Test def testFibonacci4() {
    assertEquals(3, fibonacci(4))
  }

  @Test def testFibonacci9() {
    assertEquals(34, fibonacci(9))
  }


}
