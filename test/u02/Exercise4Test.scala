package u02

import u02.Exercise4._

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class Exercise4Test {

  @Test def testTrueCurriedEvaluationLambda() {
    val partial1: Int => Int => Boolean = curriedMinorEvaluationLambda(2)
    val partial2: Int => Boolean = partial1(5)
    assertTrue(partial2(9))
  }

  @Test def testFalseCurriedEvaluationLambda() {
    val partial1: Int => Int => Boolean = curriedMinorEvaluationLambda(2)
    val partial2: Int => Boolean = partial1(5)
    assertFalse(partial2(4))
  }

  @Test def testTrueNonCurriedEvaluationLambda() {
    assertTrue(minorEvaluationLambda(2, 5, 9))
  }

  @Test def testFalseNonCurriedEvaluationLambda() {
    assertFalse(minorEvaluationLambda(2, 5, 4))
  }

  @Test def testTrueCurriedEvaluationMethod() {
    val partial1: Int => Int => Boolean = curriedMinorEvaluationMethod(2)
    val partial2: Int => Boolean = partial1(5)
    assertTrue(partial2(9))
  }

  @Test def testFalseCurriedEvaluationMethod() {
    val partial1: Int => Int => Boolean = curriedMinorEvaluationMethod(2)
    val partial2: Int => Boolean = partial1(5)
    assertFalse(partial2(4))
  }

  @Test def testTrueNonCurriedEvaluationMethod() {
    assertTrue(minorEvaluationMethod(2, 5, 9))
  }

  @Test def testFalseNonCurriedEvaluationMethod() {
    assertFalse(minorEvaluationMethod(2, 5, 4))
  }

}
