package u02

import u02.Exercise5._

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class Exercise5Test {

  @Test def testIntComposition() {
    assertEquals(9, compose[Int, Int, Int](_ - 1, _ * 2)(5))
  }

  final val SALUTE = "Ciao, "
  final val PHRASE = "mi chiamo "
  final val NAME = "Alessandro"

  @Test def testStringComposition() {
    assertEquals(SALUTE + PHRASE + NAME, compose(SALUTE + _, PHRASE + _)(NAME))
  }

  @Test def testDoubleComposition() {
    assertEquals(10.56, compose[Double, Double, Double](_ - 1.1, _ * 2.2)(5.3))
  }

  @Test def testMixedComposition() {
    assertEquals(4.3, compose[Int, Float, Double](_ + 3.3, _ / 2)(3))
  }

}
