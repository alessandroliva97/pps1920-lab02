package u02

import u02.Exercise7._

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class Exercise7Test {

  import Operations._

  val rectangle: Rectangle = Rectangle(3, 4)

  @Test def testRectanglePerimeter() {
    //assertEquals(14, rectangle.getPerimeter())
    assertEquals(14, getPerimeter(rectangle))
  }

  @Test def testRectangleArea() {
    //assertEquals(12, rectangle.getArea())
    assertEquals(12, getArea(rectangle))
  }

  val circle: Circle = Circle(5)

  @Test def testCirclePerimeter() {
    //assertEquals(31.41592653589793, circle.getPerimeter())
    assertEquals(31.41592653589793, getPerimeter(circle))
  }

  @Test def testCircleArea() {
    //assertEquals(78.53981633974483, circle.getArea())
    assertEquals(78.53981633974483, getArea(circle))
  }

  val square: Square = Square(6)

  @Test def testSquarePerimeter() {
    //assertEquals(24, square.getPerimeter())
    assertEquals(24, getPerimeter(square))
  }

  @Test def testSquareArea() {
    //assertEquals(36, square.getArea())
    assertEquals(36, getArea(square))
  }

}
