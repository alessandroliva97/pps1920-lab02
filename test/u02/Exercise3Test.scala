package u02

import u02.Exercise3._

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class Exercise3Test {

  @Test def testPositiveParityLambda() {
    assertEquals("even", parityLambda(2))
  }

  @Test def testNegativeParityLambda() {
    assertEquals("even", parityLambda(-10))
  }

  @Test def testOddParityLambda() {
    assertEquals("odd", parityLambda(7))
  }

  @Test def testPositiveParityMethod() {
    assertEquals("even", parityMethod(2))
  }

  @Test def testNegativeParityMethod() {
    assertEquals("even", parityMethod(-10))
  }

  @Test def testOddParityMethod() {
    assertEquals("odd", parityMethod(7))
  }

  val empty: String => Boolean = _ == ""
  val notEmptyL: String => Boolean = negLambda(empty)

  @Test def testNegLambda() {
    assertTrue(notEmptyL("foo") && !notEmptyL(""))
  }

  val notEmptyM: String => Boolean = negMethod(empty)

  @Test def testNegMethod() {
    assertTrue(notEmptyM("foo") && !notEmptyM(""))
  }

  @Test def testGenericNegMethod() {
    val positiveOrZero = (x: Int) => x >= 0
    val negative = negMethodGeneric(positiveOrZero)
    assertTrue(negative(-5) && !negative(2))
  }

}
