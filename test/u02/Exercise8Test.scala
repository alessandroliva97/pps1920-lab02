package u02

import u02.Optionals._

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class Exercise8Test {

  import Option._

  @Test def testFilterTrue() {
    assertEquals(Some(5), filter(Some(5))(_ > 2))
  }

  @Test def testFilterFalse() {
    assertEquals(None(), filter(Some(5))(_ > 8))
  }

  @Test def testFilterNone() {
    assertEquals(None(), filter(Some(5))(_ > 8))
  }

  @Test def testMapTrue() {
    assertEquals(Some(true), map(Some(5))(_ > 2))
  }

  @Test def testMapFalse() {
    assertEquals(Some(false), map(Some(5))(_ > 8))
  }

  @Test def testMapNone() {
    assertEquals(None(), map(None[Int]())(_ > 8))
  }

  @Test def testMap2Int() {
    assertEquals(Some(2), map2(Some(5))(Some(3))(_ - _))
  }

  final val SENTENCE = "Sono il numero "

  @Test def testMap2String() {
    assertEquals(Some(SENTENCE + 5), map2(Some(SENTENCE))(Some(5))(_ + _))
  }

  @Test def testMap2Boolean() {
    assertEquals(Some(true), map2(Some(false))(Some(true))(_ || _))
  }

  @Test def testMap2Empty() {
    assertEquals(None(), map2(Some(5))(None[Int]())(_ > _))
  }

}
